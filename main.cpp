#include <iostream>
#include "tasks.h"
#include "utils.h"

using namespace std;

typedef void (*TaskFunction)();

string generateLine(int length) {
    string line;
    for (int i = 0; i < length; i++) {
        line += "#";
    }
    return line;
}

int taskUAChooser(int limit) {

    bool isTypedRight = false;
    int result = 0;
    while (!isTypedRight) {
        cout << "Choose task (1 - " << limit << ")" << endl;
        try {
            cin.exceptions(istream::failbit | istream::badbit);
            cin >> result;
            if (result > limit || result < 1) {
                throw RuntimeException("received a bad int");
            }
            isTypedRight = true;
        } catch (...) {
            clearStreamInput();
            cout << "Please, enter only numbers (1 - " << limit << ")" << endl;
        }
        clearStreamInput();
    }
    return result;

}


int main() {
    TaskFunction taskFunctions[] = {
            firstTask,
            secondTask,
            thirdTask,
            fourthTask,
            fifthTask,
            sixthTask,
            seventhTask,
            eighthTask,
            ninthTask,
            tenthTask,
            eleventhTask,
            twelfthTask,
            thirteenthTask,
            fourteenthTask,
            fifteenthTask
    };

    string welcomeMessage = "This is Lab #1 (t.ly/xM2p)";
    cout << welcomeMessage << endl;
    cout << generateLine((int) welcomeMessage.size()) << endl;
    bool isReady = false;
    while (!isReady) {
        int a = taskUAChooser(15);
        try {
            taskFunctions[a - 1]();
        } catch (RuntimeException &e) {
            cout << "There was an Exception: " << e.what() << endl;
        }
        const string message = "Enter any key to continue or 'q' to quit:";
        cout << endl << generateLine((int) message.length()) << endl;
        isReady = exitFromCircle(message);
    }

    return 0;
}