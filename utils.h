#include <string>
#include "iostream"

using namespace std;

#ifndef UNIVERISTYC1_UTILS_H
#define UNIVERISTYC1_UTILS_H

int requireInt(string requiredText);

bool exitFromCircle(string leaveText);

void clearStreamInput();

int findMin(int i1, int i2);

int findMax(int i1, int i2);

template<typename T>
void print(T t) {
    cout << t << endl;
}

template<typename T, typename... Args>
void print(T t, Args... args) {
    cout << t << "  ";
    print(args...);
}


class RuntimeException : public exception {
    string M;
public:
    RuntimeException(const char *errorMessage) : M(errorMessage) {}

    const char *what() const noexcept override {
        return M.c_str();
    }
};

#endif //UNIVERISTYC1_UTILS_H
