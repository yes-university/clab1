#include "utils.h"
#include <iostream>
#include "stdio.h"

using namespace std;

void clearStreamInput() {
    cin.clear();
    string ignoreLine;
    getline(cin, ignoreLine);
}

int requireInt(string requiredText) {
    bool isTypedRight = false;
    int result = 0;
    while (!isTypedRight) {
        cout << requiredText;
        try {
            cin >> result;
            cin.exceptions(istream::failbit | istream::badbit);
            cout << endl;
            isTypedRight = true;
        } catch (...) {
            clearStreamInput();
            cout << "Please, enter only numbers!" << endl;
        }
        clearStreamInput();
    }
    return result;
};

int findMin(int i1, int i2) {
    if (i1 < i2) {
        return i1;
    } else {
        return i2;
    }
}

int findMax(int i1, int i2) {
    if (i1 > i2) {
        return i1;
    } else {
        return i2;
    }
}

bool exitFromCircle(string leaveText) {
    char input;
    printf(leaveText.c_str());
    scanf(" %c", &input);
    clearStreamInput();
    if (input == 'q') {
        return true;
    }
    cout << endl;
    return false;
};